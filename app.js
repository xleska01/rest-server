var func = require('./functions.js');
var register = require("./register.js");
var conf = require("./config.js");
var dbg = conf.log.dbg;

// TODO rename server
var processName = "simpleREST"







/**
 * @def registerMethods method for register REST 
 * @param app instance of express() class
 */
function registerMethods(app){
  
  
    app.get('/', function(req, res) {
    res.type('text/plain');
    res.send(200,"Simple rest api component");
    });
    
    
    app.get('/example', register(func.example));
    
    
    
    /**
     * @brief helpmethod for functions
     */
    app.get('/help', register(func.help));
    app.get('/help/:method', register(func.help));
  
  
}







/** 
 * @brief fork server to multiple processes using cluster
 */

var cluster = require('cluster');

if (cluster.isMaster) {   

    process.title=processName;
    
    cluster.on('exit', function (worker) {
        
        dbg.err4 ('Worker ' + worker.id + ' died !');
        dbg.fatal4('Worker  died !');
        dbg.info3('Forking new process to replace dead one');
        cluster.fork();
    });
    
    //fork default count
    for (var i = 0; i < conf.interface.maxProccess; i += 1) {
        cluster.fork();
    }
} else {

    process.title=processName+"-node";
    
    //create server
    var express = require('express');
    var body_parser = require('body-parser')
    var app = express();

    app.use( body_parser.json({limit: '50mb'}) );       // to support JSON-encoded bodies
    app.use( body_parser.urlencoded({limit: '50mb', extended: true}) ); // to support URL-encoded bodies
    

    registerMethods(app);

    app.listen(conf.interface.port);

}


