var utils = require("./utils.js");


function config(){
    
    var _cfgParser = new utils.configParser("./conf/server.conf");
    
    this.interface = {
        "port"                :_cfgParser.getConfigOpt("interface","Port","3360"),
        "maxProccess"         :_cfgParser.getConfigOpt("interface","maxProcess",4),
    }
    
    this.log = {
        "mask"                : _cfgParser.getConfigOpt("control","LogMask","I3W2E1F1"),
        "logfile"             : _cfgParser.getConfigOpt("control","LogFile","./logfile")
    }
    
    this.log["dbg"] = new utils.dbg(this.log.logfile, this.log.mask);
}

module.exports=new config();

