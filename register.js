var conf = require("./config.js");
var dbg = conf.log.dbg;

/**
 * @def  register 
 * @brief function wraper to handler, prepare params to function and set correct type to response
 *        catch all exceptions and return 500
 * 
 * @param method_to_call which method will be called
 */
function register(method_to_call){

  return function(req, res) {
  
  var params = {}
    
  for(var paramKey in req.body){
     params[paramKey] = req.body[paramKey]   
  }   
 
  for(var paramKey in req.query){
     params[paramKey] = req.query[paramKey]
  }
  
  for(var paramKey in req.params){
     if (! params.additionalFromUrl) params.additionalFromUrl={}
     params.additionalFromUrl[paramKey] = req.params[paramKey]
  }
  
  
  try{
  var response = method_to_call(params)
  } catch (e) { 
      stringException = ""
      try{
        dbg.dbg1("Exception throwed inside function handling '" + req.url + "' error:'" + e + "'",e);
        stringException = String(e);
      } catch (e){
          dbg.fatal4("Exception during loging exception");
      }
      res.type('text/plain');  
      res.send(500,"Internal server error " + stringException);
      return
  }

  if (typeof response !== 'string'){

    if (response.hasOwnProperty('type')){
      res.type(response.type);
    } else {
      res.type('text/plain');
    }

    var statusCode=200;
    if(response.hasOwnProperty('status')){
      statusCode=response['status']
    }
 
    if (response.hasOwnProperty('result')){
       if (typeof response['result'] !== 'string'){
         res.json(statusCode,response['result'])
       } else {
         res.send(statusCode,response['result'])
       }
    } else {
       res.json(statusCode,response)
    }
   
     
  } else {
    res.type('text/plain');  
    res.send(200,response);
  }
  }
}

module.exports = register;

