// server functions

var conf=require("./config.js");
var dbg=conf.log.dbg;
var help=require("./help/help.js");



module.exports = {

help: function(params){
   
   method=""; 
   if ( params.additionalFromUrl && params.additionalFromUrl.method){
        method = params.additionalFromUrl.method;
   }
    
   return {
        "result" : method == "" ? help.help : help[method],
        "status" : 200,
        "type"   : "application/json"
   }   
},

example: function(params){
   
   
   res={
        "result" : "This is simple example of function",
        "status" : 200,
        "type"   : "text/plain"
   }  
   
   return res
}


}


