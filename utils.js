
module.exports = {
  configParser: configParser,
  dbg:dbglogger
}

var dateFormat = require('dateformat');
var fs = require('fs');
var traceback = require('traceback');


/**
 * @def  configParser 
 * @brief object to read & parse szn style configuration file
 * 
 * @param file filename to parse
 * @param log  object to provide log facility must have log(string), default console 
 */

function configParser(file,log){

   //check log 
   log = typeof log !== 'undefined' && log.hasOwnProperty('log') && typeof log.log === 'function' ? log : console;
   
  
   this.configDict = load_szn_conf_file(file);
   if(this.configDict == false) throw "could not parse config file " + file;

   function load_szn_conf_file(file){
       var configDict = {};
       //open & read file
       

       log.log("Opening file " + file);

       var configData;
       configData = fs.readFileSync(file, 'utf8', function (err,data) {
           if (err) {
               log.log(err);
               return false;
           } else {
               return data;
           }
       });


       //return if load failed
       if (!configData){
           log.log("Couldn't load file");
           return false;
       }

       //replace comments
       configData = configData.replace(/#.*[^\n]/g,"")


       //save data to struct
       configDataArray = configData.split("\n")
       actualPart = "default"
       for(index in configDataArray){
           if (! configDataArray[index]) continue;

           var match = configDataArray[index].match(/^\[.*\]/g);
           if (match &&  match.hasOwnProperty('length') &&  match.length){
               actualPart = match[0].substring(1,match[0].length -1);
               continue;
           }

           var configLine=configDataArray[index];

           if (configLine.indexOf("=")<=0){
               log.log("Invalid line in config" + configLine);
               return false;
           }

           if (! configDict[actualPart]){
               configDict[actualPart] = {}
           }

           configDict[actualPart][configLine.substring(0,configLine.indexOf("="))] = configLine.substring(configLine.indexOf("=")+1);

       }
      return configDict;
   }

   /**
    * @def  getConfigOpt 
    * @brief object to read & parse szn style configuration file
    * 
    * @param section config file session, [session]
    * @param name  option name 
    * @param defaultValue if option not in config this value will be returned, if not set return undefined
    */
   this.getConfigOpt = function(section,name,defaultValue){
       if (this.configDict.hasOwnProperty(section) && this.configDict[section].hasOwnProperty(name)){
           return this.configDict[section][name];
       } else {
           log.log("Using default value:",name);
           return typeof defaultValue === 'undefined' ? undefined : defaultValue ;
       }
   }

}


/**
 * @def  dbglogger 
 * @brief object to provide log facility
 * 
 * @param file filename to parse
 * @param logMask default dont log anything, us "A" in mask to log all. [example "E1F1D4I4"]
 */
function dbglogger(file,logMask){
   
    this.logMask = parse(typeof logMask==='undefined' ? "" : logMask );
    if (! this.logMask) throw "Can't create dbg object!";
    this.file = file;
    
    function parse(logmask){
      
      var parsed = {"D":5,"I":5,"E":5,"F":5};
      
      logmask = logmask.toUpperCase();
      
      if(logmask.indexOf("A")<0){
          for(ch in parsed){
              index = logmask.indexOf(ch);
              if (index>=0){
                if( (index + 1) >= logmask.length){
                    console.log("Can't create dbg object, invalid logMask " + logmask);
                    return false;
                }
                    
                var val = Number(logmask[index + 1]);
                val = val > 4 ? 4 : val < 1 ? 1 : val; 
                parsed[ch]=val;
              }
          }
      } else {
          parsed = {"D":1,"I":1,"E":1,"F":1};
      }
      return parsed;
      
    }   
}

dbglogger.prototype.log = function(string,level){
        
    level=typeof level==='undefined' || level.length!=2 ? 'I4' : level;
    
    if (this.logMask[level.toUpperCase[0]] <= Number(level.toUpperCase[1])) {
        return false;
    }

    var stack = traceback();
    
    var callerString = "{" + stack[1].file +":"+ stack[1].name.split(".").pop() + "():"+ stack[1].line +"}";

    
    var timestr=dateFormat(new Date,  "yyyy-mm-dd h:MM:ss");
    fs.appendFile(this.file, timestr + " " + level + " " + string + " " + callerString + "\n" , function (err) {if (err) { console.log("Couldn't write to logfile " + err) }});
}

dbglogger.prototype._log = function(string,level,exceptionObj){
        
    level=typeof level==='undefined' || level.length!=2 ? 'I4' : level;
    
    if (this.logMask[level.toUpperCase[0]] <= Number(level.toUpperCase[1])) {
        return false;
    }

    var stack = traceback();
    var name = ! stack[2].name || stack[2].name.indexOf(".")<0 ? "unknown" : stack[2].name.split(".").pop();
    var callerString = "{" + stack[2].file +":"+ name + "():"+ stack[2].line +"}";

    exceptionString = (typeof exceptionObj !== 'undefined') ? ("\n" + exceptionObj.stack) : "";
    
    var timestr=dateFormat(new Date,  "yyyy-mm-dd h:MM:ss");
    fs.appendFile(this.file, timestr + " " + level + " " + string + " " + callerString + "\n" + exceptionString, function (err) {if (err) { console.log("Couldn't write to logfile " + err) }});
}

//TODO all can handle exception not only dbg1
//TODO it not work properly on dbg1
dbglogger.prototype.dbg1 = function(string,exceptionObj){ this._log(string, "D1", exceptionObj); }
dbglogger.prototype.dbg2 = function(string){ this._log(string,"D2"); }
dbglogger.prototype.dbg3 = function(string){ this._log(string,"D3"); }
dbglogger.prototype.dbg4 = function(string){ this._log(string,"D4"); }

dbglogger.prototype.info1 = function(string){ this._log(string,"I1"); }
dbglogger.prototype.info2 = function(string){ this._log(string,"I2"); }
dbglogger.prototype.info3 = function(string){ this._log(string,"I3"); }
dbglogger.prototype.info4 = function(string){ this._log(string,"I4"); }

dbglogger.prototype.warn1 = function(string){ this._log(string,"W1"); }
dbglogger.prototype.warn2 = function(string){ this._log(string,"W2"); }
dbglogger.prototype.warn3 = function(string){ this._log(string,"W3"); }
dbglogger.prototype.warn4 = function(string){ this._log(string,"W4"); }

dbglogger.prototype.err1 = function(string){ this._log(string,"E1"); }
dbglogger.prototype.err2 = function(string){ this._log(string,"E2"); }
dbglogger.prototype.err3 = function(string){ this._log(string,"E3"); }
dbglogger.prototype.err4 = function(string){ this._log(string,"E4"); }

dbglogger.prototype.fatal1 = function(string){ this._log(string,"F1"); }
dbglogger.prototype.fatal2 = function(string){ this._log(string,"F2"); }
dbglogger.prototype.fatal3 = function(string){ this._log(string,"F3"); }
dbglogger.prototype.fatal4 = function(string){ this._log(string,"F4"); }

